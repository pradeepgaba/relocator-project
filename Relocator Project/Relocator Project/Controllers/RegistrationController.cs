﻿using DAL;
using System.Web.Mvc;

namespace Relocator_Project.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IRegistrationOperation _registrationOperation;
        /// <summary>
        /// Method to Get the Json Data
        /// </summary>
        /// <param name="obj">Objects</param>
        /// <returns>Json Data</returns>
        private JsonResult GetData(object obj)
        {
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        public RegistrationController()
        {
            _registrationOperation = new RegistrationOperation();
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllMotivations()
        {
            return GetData(_registrationOperation.GetMotivations());
        }
        public JsonResult LoadSlectedMotivationTable(int id)
        {
            return GetData(_registrationOperation.GetMotivationSelectedTableList(id));
        }
        public JsonResult LoadWorkSectors()
        {
            return GetData(_registrationOperation.LoaWorkSectors());
        }
        public JsonResult LoadItBranches()
        {
            return GetData(_registrationOperation.LoadItBranches());
        }
        public JsonResult LoadMedicalSectors()
        {
            return GetData(_registrationOperation.LoadMedicalSectors());
        }
    }

}
