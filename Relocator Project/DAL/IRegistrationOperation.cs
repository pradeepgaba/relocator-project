﻿using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public interface IRegistrationOperation
    {
        /// <summary>
        /// Get all Motivation records from Motivation
        /// </summary>
        /// <returns></returns>
        IQueryable<Motivation> GetMotivations();

        /// <summary>
        /// Get Motivation Table by Id. Return type is object, because records will return from many tables.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        object GetMotivationSelectedTableList(int id);

        /// <summary>
        /// Load WorkSector table.
        /// </summary>
        /// <returns></returns>
        IQueryable<WorkSector> LoaWorkSectors();

        /// <summary>
        /// Load ITBranch table.
        /// </summary>
        /// <returns></returns>
        IQueryable<ITBranch> LoadItBranches();

        /// <summary>
        /// Load MedicalSector table
        /// </summary>
        /// <returns></returns>
        IQueryable<MedicalSector> LoadMedicalSectors();
    }
}
