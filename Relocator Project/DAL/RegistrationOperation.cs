﻿using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class RegistrationOperation : IRegistrationOperation
    {
        private readonly ReclocatorDBEntities _relocatorDbEntities;
        public RegistrationOperation()
        {
            _relocatorDbEntities = new ReclocatorDBEntities();
        }
        public IQueryable<Motivation> GetMotivations()
        {
            var motivations = from m in _relocatorDbEntities.Motivations select m;
            return motivations;
        }
        public object GetMotivationSelectedTableList(int id)
        {
            var getRecords = GetMotimationRelatedRecords(id);
            if (getRecords == null) return null;
            return getRecords;
        }
        private object GetMotimationRelatedRecords(int id)
        {
            object getRecords;
            switch (id)
            {
                case 1:
                    getRecords = from m in _relocatorDbEntities.Works select m;
                    break;
                case 2:
                    getRecords = from m in _relocatorDbEntities.Entrepeneurs select m;
                    break;
                case 3:
                    getRecords = from m in _relocatorDbEntities.Studies select m;
                    break;
                case 4:
                    getRecords = from m in _relocatorDbEntities.Families select m;
                    break;
                case 5:
                    getRecords = from m in _relocatorDbEntities.Tourism select m;
                    break;
                default:
                    return null;
            }
            return getRecords;
        }
        public IQueryable<WorkSector> LoaWorkSectors()
        {
            var records = from v in _relocatorDbEntities.WorkSectors select v;
            return records;
        }
        public IQueryable<ITBranch> LoadItBranches()
        {
            var records = from v in _relocatorDbEntities.ITBranches select v;
            return records;
        }
        public IQueryable<MedicalSector> LoadMedicalSectors()
        {
            var recods = from v in _relocatorDbEntities.MedicalSectors select v;
            return recods;
        }
    }
}
